extern crate reqwest;

use super::BASE_URL;
use reqwest::{
    header::{HeaderMap as Headers, HeaderValue, AUTHORIZATION},
    Client,
};
use serde::Deserialize;
use std::collections::HashMap;
use std::sync::Arc;

const GATEWAY: &'static str = "/gateway/bot";

pub struct HTTPClient {
    client: Arc<Client>,
    pub base_url: String,
    pub token: String,
}

pub struct GatewayPayload {
    pub opcode: i32,
    pub data: HashMap<String, i32>,
    pub sequence: i32,
    pub event_name: String,
}

pub struct Gateway {
    pub heartbeat_interval: i32,
}

pub struct SessionStartLimit {
    pub start: u32,
    pub remaining: u32,
    pub reset_after: u32,
}

#[derive(Deserialize)]
struct BotGatewayResponse {
    pub url: String,
    pub shards: u32,
    pub session_start_limit: SessionStartLimit,
}

impl HTTPClient {
    pub fn new(client: Arc<Client>, token: &str) -> Self {
        HTTPClient {
            client: Arc::clone(&client),
            base_url: BASE_URL.to_string(),
            token: token.to_string(),
        }
    }

    pub fn new_with_token(token: &str) -> Result<Self, String> {
        let mut headers = Headers::new();
        headers.append(
            AUTHORIZATION,
            HeaderValue::from_str(&format!("Bot {}", &token)).unwrap(),
        );

        let builder = Client::builder();
        match builder.default_headers(headers).build() {
            Ok(client) => Ok(HTTPClient::new(Arc::new(client), token)),
            Err(err) => Err(err.to_string()),
        }
    }

    pub async fn get_gateway(&self, url: &str) -> Result<BotGatewayResponse, reqwest::Error> {
        self.client
            .get(&(self.base_url + &GATEWAY.to_string()))
            .send()
            .await?
            .json::<BotGatewayResponse>()
            .await
    }
}

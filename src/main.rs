#[macro_use]
extern crate slog;
extern crate slog_async;
extern crate slog_term;

#[macro_use]
extern crate serde;

mod http;
mod ws;

use http::client::HTTPClient;
use slog::Drain;

fn main() {
    let decorator = slog_term::TermDecorator::new().build();
    let drain = slog_term::FullFormat::new(decorator).build().fuse();
    let drain = slog_async::Async::new(drain).build().fuse();

    let root = slog::Logger::root(drain, o!());

    info!(root, "Y'all ready to drop some memes? (☞ ﾟヮﾟ)☞");

    let client = match HTTPClient::new_with_token(
        "NzM4OTg5MjU0OTIyODYyNjIz.XyT7YA.21rZ_seE3UFEsYQc3A2TNH_4XXU",
    ) {
        Ok(client) => client,
        Err(err) => return,
    };

    info!(root, "Shutting down...");
}
